from fabric.api import run, env

#ssh -i ~/.ssh/AWS-ctrl-sfcoll.pem ubuntu@54.191.239.172
#fab -f controller_sflow.py ctrl_start mntr_start

#Start my FloodLight controller and sFlow server on my AWS Ubuntu


env.user = 'ubuntu'
env.key_filename = '~/.ssh/AWS-ctrl-sfcoll.pem'

env.hosts = '54.191.239.172'

# Start FloodLight with no stdout print
def ctrl_start():
    run('nohup java -jar floodlight-0.90.jar >& /dev/null < /dev/null &', pty=False)

def mntr_start():
    run('cd sflow-rt/; ./start.sh &')
