from fabric.api import run, env

#####################
# How to run this
#####################
#fab -f fabfile.py list1 traffic
################################

# Login to each VM on the UtahDDC sliver 
#and run ping to another node - 
#IP addresses need to be automatically loaded 
#either from ifconfig or better from 
#original script that instantiated these nodes in the first place. 

env.user = 'dgurka01'
env.key_filename = '~/.ssh/dgurkan_geni_ssh_rsa.pub'

hosts = ['pc7.utahddc.geniracks.net:33338', 'pc7.utahddc.geniracks.net:33339', 'pc7.utahddc.geniracks.net:33340']

def list1():
    env.hosts = hosts[0] 

def traffic1():
   run('nohup ping 10.40.1.2 >& /dev/null < /dev/null &', pty=False)

def list2():
    env.hosts = hosts[1] 

def traffic2():
   run('nohup ping 10.40.1.3 >& /dev/null < /dev/null &', pty=False)

def list3():
    env.hosts = hosts[2]

def traffic3():
   run('nohup ping 10.40.1.1 >& /dev/null < /dev/null &', pty=False)
