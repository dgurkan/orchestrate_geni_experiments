from geni.aggregate import FrameworkRegistry
from geni.aggregate.context import Context
from geni.aggregate.user import User

def buildContext ():
  portal = FrameworkRegistry.get("portal")()
  portal.cert = "/Users/dgurkan/.ssl/geni-dgurka01.pem"
  portal.key = "/Users/dgurkan/.ssl/geni-dgurka01.pem"

  dgurkan = User()
  dgurkan.name = "dgurkan"
  dgurkan.urn = "urn:publicid:IDN+ch.geni.net+user+dgurka01"
  dgurkan.addKey("/Users/dgurkan/.ssh/dgurkan_geni_ssh_rsa.pub")

  context = Context()
  context.addUser(dgurkan, default = True)
  context.cf = portal
  context.project = "VTS-experiments"

  return context
