import dgurkan_config
import itertools
import time
import geni.rspec.vts as VTS
import geni.rspec.pg as PG
import geni.aggregate.instageni as IG
import geni.aggregate.core as CORE
import geni.aggregate.context as CONTEXT
import geni.aggregate.user as USER
import geni.aggregate.vts as VTSAM
import geni.rspec.pgmanifest as PGM
import geni.rspec.vtsmanifest as VTSM
from geni.aggregate.apis import DeleteSliverError
from geni import util

context = dgurkan_config.buildContext()
SLICE='gec21ple'

#########
#########

#try:
#  VTSAM.DDC.deletesliver(context, SLICE)
#except DeleteSliverError:
#  pass
#
##########
##########
#vtsr = VTS.Request()
#controller_address = "54.191.239.172"
#controller_url = "tcp:%s:%d" % (controller_address, 6633) # Default port is 6633
#image = VTS.OVSOpenFlowImage(controller_url, ofver="1.0")
#sflow = VTS.SFlow(controller_address)
#sflow.collector_port = 6343 # Default is 6343
#image.sflow = sflow
#
#dps = [VTS.Datapath(image, "dp%d" % (x)) for x in xrange(0,4)]
#for dp in dps:
#  vtsr.addResource(dp)
#VTS.connectInternalCircuit(dps[0],dps[2])
#VTS.connectInternalCircuit(dps[1],dps[2])
#VTS.connectInternalCircuit(dps[2],dps[3])
#dps[0].attachPort(VTS.PGCircuit())
#dps[1].attachPort(VTS.PGCircuit())
#dps[3].attachPort(VTS.PGCircuit())
#
#vtsr.write('%s_req_VTS.xml'%SLICE)
#
#vtsm = VTSAM.DDC.createsliver(context, SLICE, vtsr)
#print vtsm.text
name_f_vts="%s-%s-manifest.xml" %(SLICE,'VTSAM')
#f = open(name_f_vts, "w+")
#f.write(vtsm.text)
#f.close()
##########
##########
#print '''
#****** now here *****
#building UtahDDC VMs and their connectivity
#*********************'''

#try:
#  IG.UtahDDC.deletesliver(context, SLICE)
#except DeleteSliverError:
#  pass
#
#vtsm = VTSM.Manifest(path=name_f_vts)
NUM_VMS = 3
#tr = PG.Request()
#IP = "10.40.1.%d"
#for idx, circuit in enumerate(vtsm.pg_circuits):
#  vm = PG.XenVM("host%d" % (idx))
#  vm_iface = vm.addInterface("if0")
#  vm_iface.addAddress(PG.IPv4Address(IP % (idx + 1), "255.255.255.0"))
#  tr.addResource(vm)
#  lnk = PG.Link()
#  lnk.addInterface(vm_iface)
#  lnk.connectSharedVlan(circuit)
#  tr.addResource(lnk)
#
#tr.write('%s_req_IG.xml'%SLICE)
#m = IG.UtahDDC.createsliver(context, SLICE, tr)
#print m.text
name_f="%s-%s-manifest_total.xml" % (SLICE,"UtahDDC")
#f = open(name_f, "w+")
#f.write(m.text)
#f.close()

##########
print '''
Given VTS and PG resources are reserved and connected
****** manifests are in local directory
read manifests to determine how to login to nodes
populate fabric file to run experiments with traffic generation'''

manifest = PGM.Manifest(path=name_f)
util.printlogininfo(manifest = manifest)

f = open("fabfile.text", "w+")
f.write("from fabric.api import run, env \n\n")
f.write("env.key_filename = '~/.ssh/dgurkan_geni_ssh_rsa.pub' \n")
n=0 #count the number of hosts to be written into fabfile.text
for node in manifest.nodes:
	for login in node.logins:
		if n==0:	
			f.write("env.key_filename = '~/.ssh/dgurkan_geni_ssh_rsa.pub' \n\n")
			f.write("env.user = '%s' \n\n" % login.username)
	     		f.write("hosts = ['%s:%d', " % (login.hostname, login.port))
			n=1
	      	elif 0<n<NUM_VMS-1:
			f.write("'%s:%d', " % (login.hostname, login.port))
			n+=1
	      	else:
			f.write("'%s:%d']\n\n" % (login.hostname, login.port))
			f.write("# Task definitions come in here: \n\n")
			f.write("# To run this fabric file \n\n")
			f.write("# fab -f fabfile.py list1 traffic")

f.close()




