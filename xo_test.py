import dgurkan_config
import itertools
import time
import geni.rspec.vts as VTS
import geni.rspec.pg as PG
import geni.aggregate.instageni as IG
import geni.aggregate.exogeni as EG
import geni.aggregate.core as CORE
import geni.aggregate.context as CONTEXT
import geni.aggregate.user as USER
import geni.aggregate.vts as VTSAM
import geni.rspec.egext as EGX

# One node OVS instance in instageni
context = dgurkan_config.buildContext()
xo_request = PG.Request()
SLICE='streamline'
NUM_VMS = 2

##########
##########

vm1 = EGX.XOSmall("vm1")
#vm.disk_image = EGX.DiskImage("http://geni-images.renci.org/images/standard/debian/deb6-neuca-v1.0.6.xml", "4ad567ce3b1c0dbaa15bad673bbf556a9593e1c")
vm1if = vm1.addInterface("if0")
xo_request.addResource(vm1)
vm2 = EGX.XOSmall("vm2")
vm2if = vm2.addInterface("if0")
xo_request.addResource(vm2)

lnk = PG.LAN('lan0')
lnk.addInterface(vm1if)
lnk.addInterface(vm2if)
xo_request.addResource(lnk)

xo_request.write('xo_test.xml')
manifest = EG.UH.createsliver(context, "streamline", xo_request)
print manifest.text

# At this point you'll want to poll the sliverstatus from FIU to wait
# until the VM's are ready
while True:
    try:
        ss = EG.UH.sliverstatus(context, SLICE)
        if ss['pg_status'] == 'ready': break
    except:
        pass # Sometimes we get a 'busy - try again later' exception
    time.sleep(10)
